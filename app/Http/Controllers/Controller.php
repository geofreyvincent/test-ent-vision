<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //
    public function __construct(){
        $this->guzzle_ent = new Client([
            'base_uri' => env('GUZZLE_BASE_URI', 'http://devtest.ent-vision.com'),
            'headers' => [
                'Content-Type'  => 'application/json'
            ],
        ]);

        $this->guzzle_tms_ent = new Client([
            'base_uri' => env('GUZZLE_BASE_URI', 'http://devtest.ent-vision.com'),
            'headers' => [
                'Content-Type'  => 'application/json'
            ],
        ]);
    }
}

