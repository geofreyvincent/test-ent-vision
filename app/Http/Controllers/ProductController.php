<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Http\Requests;
use DataTables;
use Validator;

class ProductController extends Controller
{
    //
    public function index(){
        return view('product_list')->withTitle('Product List');
    }
    public function view_product_page(){
        try {
            $product_list_json = $this->guzzle_ent->get('wms/GetAllProducts');
            $product_list = json_decode($product_list_json->getBody()->getContents());
            return DataTables::of($product_list)->toJson();
        } catch (Exception $e) {
            return response()->json(array(
                'data'          => [],
                'status'        => 500
            ));
        }
    }
    public function view_google_chart(){
        try {
            $product_list_json = $this->guzzle_ent->get('wms/GetAllProducts');
            $product_list = json_decode($product_list_json->getBody()->getContents());
            $category_list = array_column($product_list, 'CategoryName');
            $time_start = microtime(true);
            $existing_detail = "";
            $current_detail = "";
            $key = -1;
            $counter = 1;
            $new_array = [];
            sort($category_list);
            foreach($category_list as $index => $detail){
                if($existing_detail!=$detail){                
                    $existing_detail=$detail;
                    $counter = 1;
                    $key += 1;
                    $items_data = array(
                        'category_name'      => $existing_detail,
                        'jumlah'            => $counter
                    );
                    array_push($new_array, $items_data);
                }else{
                    $counter += 1;
                    $new_array[$key]['jumlah'] = $counter;
                }                
            }
        } catch (Exception $e) {
            return response()->json(array(
                'data'          => [],
                'status'        => 500
            ));
        }
        $result = json_encode($new_array);
        return view('product_google_chart', compact('result'))
                ->withTitle('Google Chart');
    }
    public function order_product(){
        $product_list_json = $this->guzzle_ent->get('wms/GetAllProducts');
        $product_list = json_decode($product_list_json->getBody()->getContents());
        $category_name = array_unique(array_column($product_list, 'CategoryName'));
        return view('product_order', compact('category_name'))->withTitle('Order Product');
    }
    public function view_product_page_per_category(Request $request){
        try {
            $category_name = $request->get('category_name');
            $json_body = [
                "categoryName"          => $category_name
            ];
            $products_by_category = $this->guzzle_ent->post('wms/ProductsByCategory', [
                'json' => $json_body
            ]); 
            $product_list = json_decode($products_by_category->getBody()->getContents());
            return DataTables::of($product_list)
                ->addColumn('action', function($product_list) {
                    return 
                    '
                    <input type="hidden" id="product_name'.$product_list->PartNumber.'" value="'.$product_list->ProductName.'">
                    <input type="hidden" id="units_in_stock'.$product_list->PartNumber.'" value="'.$product_list->UnitsInStock.'">
                    <button title="Add" class="add_to_cart" id="add_to_cart' . $product_list->PartNumber . '" class="btn btn-info btn-md">Add To Cart
                    </button>
                    ';
                })
                ->toJson();
        } catch (Exception $e) {
            return response()->json(array(
                'data'          => [],
                'status'        => 500
            ));
        }
    }
    public function submit_order(Request $request)
    {
        $params = $request->all();

        $rules = [
            'part_no'                   => 'required',
            'qty_product'               => 'required',
            'ShipAddress'               => 'required'
        ];
        $messages = [
            'part_no.*' => 'Product must be chosen at least one.',
            'qty_product.*' => 'Quantity must be filled.',
            'ShipAddress.*' => 'Ship Address must be filled.'
        ];

        $validator    = Validator::make($params, $rules, $messages);
        if( $validator->fails() ) {
            return response()->json(array(
                'message' => (string)$validator->errors(),
                'status'  => 403
            ));
        }
        $array_part_number = $request->get('part_no');
        $array_quantity = $request->get('qty_product');
        $itemsData = [];
        foreach($array_part_number as $key=>$array_part_number){
            $itemData = array(
                'PartNumber'            => $array_part_number,
                'Quantity'              => $array_quantity[$key]
              );
              array_push($itemsData, $itemData);
        }

        $json_body = [
            "RequiredDate" => $request->get('RequiredDate'),
            "ShipName" => $request->get('ShipName'),
            "ShipAddress" => $request->get('ShipAddress'),
            "ShipCity" => $request->get('ShipCity'),
            "ShipPostalCode" => $request->get('ShipPostalCode'),
            "ShipCountry" => $request->get('ShipCountry'),
            "CustomerID" => $request->get('CustomerID'),
            "orderDetails"  => $itemsData,
        ];
        $vorder = [
            "vorder" => $json_body
        ];
        $response_product_order = $this->guzzle_ent->post('tms/PlaceTransportOrder', [
            'json' => $vorder
        ]);
        $product_order_status =  (string) $response_product_order->getBody();
        return response()->json(array(
            'message' => $product_order_status,
            'status'  => 200
        ));
    }
}
