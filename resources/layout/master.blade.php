<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>@yield('title') | Ent-Vision</title>
</head>
<body>
    @include('layout.header')
    @yield('content')
    </body>
    <script src="{{ asset('themes/js/vendor/jquery-2.2.4.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="{{ asset('themes/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('themes/js/easing.min.js') }}"></script>
    <script src="{{ asset('themes/js/hoverIntent.js') }}"></script>
    <script src="{{ asset('themes/js/superfish.min.js') }}"></script>	
    <script src="{{ asset('themes/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('themes/js/jquery.magnific-popup.min.js') }}"></script>	
    <script src="{{ asset('themes/js/owl.carousel.min.js') }}"></script>	
    <script src="{{ asset('themes/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('themes/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('themes/js/parallax.min.js') }}"></script>
    <script src="{{ asset('themes/js/mail-script.js') }}"></script>	
    <script src="{{ asset('themes/js/main.js') }}"></script>	
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>	
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js" charset="utf-8"></script>
    @stack('scripts')
</html>