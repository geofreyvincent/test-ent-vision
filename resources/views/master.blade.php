<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>@yield('title') | Ent-Vision</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('themes/css/linearicons.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/css/social-media.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" rel="stylesheet" />
</head>
<body>
    @include('header')
    @yield('content')
    </body>
    <script src="{{ asset('themes/js/vendor/jquery-2.2.4.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="{{ asset('themes/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('themes/js/easing.min.js') }}"></script>
    <script src="{{ asset('themes/js/hoverIntent.js') }}"></script>
    <script src="{{ asset('themes/js/superfish.min.js') }}"></script>	
    <script src="{{ asset('themes/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('themes/js/jquery.magnific-popup.min.js') }}"></script>	
    <script src="{{ asset('themes/js/owl.carousel.min.js') }}"></script>	
    <script src="{{ asset('themes/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('themes/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('themes/js/parallax.min.js') }}"></script>
    <script src="{{ asset('themes/js/mail-script.js') }}"></script>	
    <script src="{{ asset('themes/js/main.js') }}"></script>	
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>	
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js" charset="utf-8"></script>
    @stack('scripts')
</html>