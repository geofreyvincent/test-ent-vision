@extends('master')

@section('title')
    {{ $title }}
@endsection

@section('content')
<!-- start banner Area -->
<section class="banner-area relative" id="home">	
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="col-lg-12">
                <h1 class="text-white text-center">
                    Product				
                </h1>											
        </div>  
    </div>			
    </div>
</section>
<!-- End banner Area -->	

<!-- Start post Area -->
<section class="post-area section-gap">
    <div class="container">
        <div class="row justify-content-center d-flex">        
            <div class="col-lg-12 post-list">
                <div class="table-responsive">
                <br>
                <a class="primary-btn-ent-vision" 
                    href="{{ url('view_google_chart') }}">
                        Product Google Chart</a>
                <a class="primary-btn-ent-vision" 
                    href="{{ url('/') }}">
                        Product List</a>
                <div class="form-group row">
                    <br>
                </div>
                <div class="col-md-15">
                    <div class="errorTxt"></div>
                </div>
                <form>
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">Required Date</label>
                        <div class="col-md-6">
                            <input type="date" id="RequiredDate" name="RequiredDate" 
                            class="form-control no-border-radius" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">Ship Name</label>
                        <div class="col-md-6">
                            <input type="text" id="ShipName" name="ShipName" 
                            class="form-control no-border-radius" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">Ship Address</label>
                        <div class="col-md-6">
                        <textarea rows="3" id="ShipAddress" required name="ShipAddress" 
                            class="form-control no-border-radius"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">Ship City</label>
                        <div class="col-md-6">
                            <input type="text" id="ShipCity" name="ShipCity" 
                                class="form-control no-border-radius" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">Ship Postal Code</label>
                        <div class="col-md-6">
                            <input type="text" id="ShipPostalCode" name="ShipPostalCode" 
                                class="form-control no-border-radius" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">Ship Country</label>
                        <div class="col-md-6">
                            <input type="text" id="ShipCountry" name="ShipCountry" 
                                class="form-control no-border-radius" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">Customer ID</label>
                        <div class="col-md-6">
                            <input type="text" id="CustomerID" name="CustomerID" 
                                class="form-control no-border-radius" value="">
                        </div>
                    </div>
                    <table class="table table-bordered table-striped" 
                    id="order-table">   
                        <thead>
                            <tr>
                                <th>Part Number</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                            </tr>
                        </thead>                 
                    </table>
                    <div class="form-group">
                        <button type="button" id="submit-create" 
                        class="btn btn-info no-border-radius">Submit Order</button>
                    </div>
                </form>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Category Name</label>
                    <div class="col-md-6">
                    <select id="category_name" 
                        class="form-control no-border-radius" name="category_name">
                        <option value="" disabled selected>Default</option>
                        @foreach ($category_name as $detail)
                            <option value="{{ $detail }}">
                                {{ $detail }}
                            </option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <table class="table table-bordered table-striped" 
                id="product-table">
                    <thead>
                        <tr>
                            <th>Part Number</th>
                            <th>Product Name</th>
                            <th>Quantity Per Unit</th>
                            <th>Unit Price</th>
                            <th>Category Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>                   
                </table>
                </div>
            </div>
        </div>
    </div>	
</section>
@endsection
<!-- End post Area -->
@push('scripts')
<script>
    $(document).ready(function() {
        
        $('#category_name').change(function() {
            var category_name = $('#category_name').val();
            $('#product-table').DataTable( {
                language:{
                    searchPlaceholder: "Search.."
                },
                destroy: true,
                processing: true,
                serverSide: true,
                ajax: '{!! url('api/product_cat') !!}?&category_name='+category_name,
                columns: [
                    { data: 'PartNumber', name: 'PartNumber' },
                    { data: 'ProductName', name: 'ProductName' },
                    { data: 'QuantityPerUnit', name: 'QuantityPerUnit' },
                    { data: 'UnitPrice', name: 'UnitPrice' },
                    { data: 'CategoryName', name: 'CategoryName' },
                    { data: 'action', name: 'action' },
                ],
            }); 
        });
        $(document).on('input', '.numeric' , function(event) {
            if(event.which >= 37 && event.which <= 40) return;
            $(this).val(function(index, value) {
                return value.replace(/[^0-9\.]+/g, '');
            });
        });
        $(document).on('input', '.qty_product' , function(event) {
            var id_input = this.id;
            var id_for_edit = id_input.replace("qty_product", "");
            var max_qty_product = parseInt($("#max_qty_product"+ id_for_edit).val());
            var current_value = parseInt($(this).val());
            if(max_qty_product < current_value){
                $(this).val(max_qty_product);
                $(this).text(max_qty_product);
            };
        });
        var rowNum = 0;
        $(document).on('click', '.add_to_cart', function(event){
            rowNum++;
            var previousNum = rowNum-1;
            var id_input = this.id;
            var id_for_edit = id_input.replace("add_to_cart", "");
            if($('table:has(tr.add-new-row'+ id_for_edit +')').length == 0){
                var product_name = $("#product_name"+ id_for_edit).val();
                var max_quantity = $("#units_in_stock"+ id_for_edit).val();
                var html= '<tr class="add-new-row'+ id_for_edit +'" id="add-new-row'+ rowNum +'">';
                html += ' <td><input type="text" id="part_no'+ rowNum +'" name="part_no[]" value="'+ id_for_edit +'" readonly class="form-control"></td>';
                html += ' <td><input type="text" id="product_name'+ rowNum +'" name="product_name[]" value="'+ product_name +'" readonly class="form-control"></td>';
                html += ' <td><input type="text" id="qty_product'+ rowNum +'" name="qty_product[]" value="0" class="numeric form-control text-right qty_product"></td>';
                html += ' <input type="hidden" id="max_qty_product'+ rowNum +'" name="max_qty_product[]" value="' + max_quantity + '" class="numeric form-control text-right max_qty_product">';
                html += ' </tr>';
                $('#order-table').append(html);
                var afterNum = rowNum+1;
            }
        });
        $(document).on('click', '#submit-create', function(event){
            
            event.preventDefault();
            $('.errorTxt').html("");

            var RequiredDate = $("#RequiredDate").val();
            var ShipName = $("#ShipName").val();
            var ShipAddress = $("#ShipAddress").val();
            var ShipCity = $("#ShipCity").val();
            var ShipPostalCode = $("#ShipPostalCode").val();
            var ShipCountry = $("#ShipCountry").val();
            var CustomerID = $("#CustomerID").val();

            //part_no_array
            var part_no = $("input[name='part_no\\[\\]']")
            .map(function(){return $(this).val();}).get();

            //qty_product_array
            var qty_product = $("input[name='qty_product\\[\\]']")
            .map(function(){return $(this).val();}).get();
            $.post('{{ route("submit-order") }}', {
                    "_token": "{{ csrf_token() }}",
                    "RequiredDate": RequiredDate,
                    "ShipName": ShipName,
                    "ShipAddress": ShipAddress,
                    "ShipCity": ShipCity,
                    "ShipPostalCode": ShipPostalCode,
                    "ShipCountry": ShipCountry,
                    "CustomerID": CustomerID,
                    "part_no": part_no,
                    "qty_product": qty_product
                    })
                    .then(function(response){
                        if(response.status == 200) {   
                            event.stopImmediatePropagation();
                            alert(response.message);
                            window.location.reload();
                        }
                        else {
                            $('.errorTxt').append('Error Order Product');
                            var error_messages = JSON.parse(response.message); 
                            var html_error_header='<div class="alert alert-danger no-border-radius">';
                            html_error_header += '<ul style="list-style: none;">';   
                            $.each( error_messages, function( key, value ) {
                                html_error_header += '<li>';
                                html_error_header += value;
                                html_error_header += '</li>';
                            });
                            html_error_header += '</ul>';
                            html_error_header += '</div>';
                            $('.errorTxt').append(html_error_header);
                        }           
                });
        });
    });
</script>
@endpush
