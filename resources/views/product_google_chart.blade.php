@extends('master')

@section('title')
    {{ $title }}
@endsection

@section('content')
<!-- start banner Area -->
<section class="banner-area relative" id="home">	
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="col-lg-12">
                <h1 class="text-white text-center">
                    Product				
                </h1>											
        </div>  
    </div>			
    </div>
</section>
<!-- End banner Area -->	

<!-- Start post Area -->
<section class="post-area section-gap">
    <div class="container">
        <div class="row justify-content-center d-flex">        
            <div class="col-lg-12 post-list">
                <div class="table-responsive">
                <br>
                <a class="primary-btn-ent-vision" 
                    href="{{ url('/') }}">
                        View Product</a>
                <a class="primary-btn-ent-vision" 
                    href="{{ url('order_product') }}">
                        Order Product</a>
                <div id="line_chart" style="width:750px; height:450px;">
                </div>
            </div>
        </div>
    </div>	
</section>
@endsection
<!-- End post Area -->
@push('scripts')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
product_list = <?php echo $result; ?>;
var product_data = product_list;  

google.charts.load('current', {
    callback: drawChart,
    packages: ['corechart', 'bar']
});

function drawChart() {
    var dataTable = new google.visualization.DataTable();
    dataTable.addColumn('string', 'category_name');
    dataTable.addColumn('number', 'jumlah');
    $.each( product_data, function( i, val ) {
        dataTable.addRows([
            [product_data[i]['category_name'], product_data[i]['jumlah']]
        ]);  
    });
    var container = document.getElementById('line_chart');
    var chart = new google.visualization.BarChart(container);
    chart.draw(dataTable);
}
</script>
@endpush
