@extends('master')

@section('title')
    {{ $title }}
@endsection

@section('content')
<!-- start banner Area -->
<section class="banner-area relative" id="home">	
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="col-lg-12">
                <h1 class="text-white text-center">
                    Product				
                </h1>											
        </div>  
    </div>			
    </div>
</section>
<!-- End banner Area -->	

<!-- Start post Area -->
<section class="post-area section-gap">
    <div class="container">
        <div class="row justify-content-center d-flex">        
            <div class="col-lg-12 post-list">
                <div class="table-responsive">
                <br>
                <a class="primary-btn-ent-vision" 
                    href="{{ url('view_google_chart') }}">
                        Product Google Chart</a>
                <a class="primary-btn-ent-vision" 
                    href="{{ url('order_product') }}">
                        Order Product</a>
                <table class="table table-bordered table-striped" 
                id="product-table">
                    <thead>
                        <tr>
                            <th>Part Number</th>
                            <th>Product Name</th>
                            <th>Quantity Per Unit</th>
                            <th>Unit Price</th>
                            <th>Category Name</th>
                        </tr>
                    </thead>                   
                </table>
                </div>
            </div>
        </div>
    </div>	
</section>
@endsection
<!-- End post Area -->
@push('scripts')
<script>
    $(document).ready(function() {
        $('#product-table').DataTable( {
            language:{
                searchPlaceholder: "Search.."
            },
            processing: true,
            serverSide: true,
            ajax: '{!! url('api/product') !!}',
            columns: [
                { data: 'PartNumber', name: 'PartNumber' },
                { data: 'ProductName', name: 'ProductName' },
                { data: 'QuantityPerUnit', name: 'QuantityPerUnit' },
                { data: 'UnitPrice', name: 'UnitPrice' },
                { data: 'CategoryName', name: 'CategoryName' },
            ],
        } );
    });
</script>
@endpush
