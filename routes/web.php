<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', 'ProductController@index')->name('product_list');

Route::get('/view_google_chart', 'ProductController@view_google_chart')->name('view_google_chart');

Route::get('/order_product', 'ProductController@order_product')->name('order_product');