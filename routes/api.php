<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/product', 'ProductController@view_product_page')->name('Product');
Route::get('/product_cat', 'ProductController@view_product_page_per_category')->name('ProductCat');

Route::post('/submit_order', 'ProductController@submit_order')->name('submit-order');
